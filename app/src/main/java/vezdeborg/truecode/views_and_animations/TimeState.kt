package vezdeborg.truecode.views_and_animations

data class TimeState(var time: Long, var isPlayed: Boolean)